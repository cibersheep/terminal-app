# Basque translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-20 09:02+0000\n"
"PO-Revision-Date: 2019-07-06 22:48+0000\n"
"Last-Translator: Aitzol Berasategi <aitzol@disroot.org>\n"
"Language-Team: Basque <https://translate.ubports.com/projects/ubports/"
"terminal-app/eu/>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.6.1\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"

#: ../src/app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Hautatu"

#: ../src/app/qml/AlternateActionPopover.qml:73
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Kopiatu"

#: ../src/app/qml/AlternateActionPopover.qml:79
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Itsatsi"

#: ../src/app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:99
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:151
#: ../src/app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Fitxa berria"

#: ../src/app/qml/AlternateActionPopover.qml:104
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:109
#, fuzzy
msgid "Close App"
msgstr "Itxi"

#: ../src/app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Autentifikazioa behar da"

#: ../src/app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Sartu gako edo pasaesaldia:"

#: ../src/app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "gakoa edo pasaesaldia"

#: ../src/app/qml/AuthenticationDialog.qml:58
#, fuzzy
msgid "Authenticate"
msgstr "Autentifikazioak huts egin du"

#: ../src/app/qml/AuthenticationDialog.qml:70
#: ../src/app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Utzi"

#: ../src/app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Autentifikazioak huts egin du"

#: ../src/app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr ""

#: ../src/app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""

#: ../src/app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr ""

#: ../src/app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Aldatu teklatua"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr "Kontrol teklak"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr "Funtzio Teklak"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "Desplazamendu Teklak"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr "Agindu Teklak"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr ""

#. TRANSLATORS: This is the name of the Control key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "CTRL"

#. TRANSLATORS: This is the name of the Alt key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr ""

#. TRANSLATORS: This is the name of the Shift key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr ""

#. TRANSLATORS: This is the name of the Escape key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr ""

#. TRANSLATORS: This is the name of the Page Up key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr ""

#. TRANSLATORS: This is the name of the Page Down key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr ""

#. TRANSLATORS: This is the name of the Delete key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr ""

#. TRANSLATORS: This is the name of the Home key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr ""

#. TRANSLATORS: This is the name of the End key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr ""

#. TRANSLATORS: This is the name of the Tab key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:115
#, fuzzy
msgid "Tab"
msgstr "Fitxak"

#. TRANSLATORS: This is the name of the Enter key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr ""

#: ../src/app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "Ados"

#: ../src/app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:66
#, fuzzy
msgid "Font:"
msgstr "Letra-tamaina:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Letra-tamaina:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Berdea beltzaren gainean"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Txuria beltzaren gainean"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Beltza txuriaren gainean"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Beltza ausazko argiaren gainean"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "Retro terminal aparta"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:131
#, fuzzy
msgid "Dark pastels"
msgstr "Pastel ilunak / Ubuntu (zaharra)"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "Beltza hori leunaren gainean"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Antolaketak"

#: ../src/app/qml/Settings/SettingsPage.qml:33
#, fuzzy
msgid "close"
msgstr "Itxi"

#: ../src/app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr ""

#: ../src/app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr ""

#: ../src/app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:145
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:150
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:155
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:160
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:165
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:156
#, fuzzy
msgid "Close terminal"
msgstr "Terminala"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:171
#, fuzzy
msgid "Next tab"
msgstr "Fitxa berria"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:175
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:185
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:190
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:195
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:200
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:205
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:210
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr ""

#: ../src/app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr ""

#: ../src/app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr ""

#: ../src/app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr ""

#: ../src/app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Fitxak"

#: ../src/app/qml/TerminalPage.qml:63 ubuntu-terminal-app.desktop.in.in.h:1
msgid "Terminal"
msgstr "Terminala"

#: ../src/app/qml/TerminalPage.qml:302
msgid "Selection Mode"
msgstr "Hautapen modua"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:291
#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:320
msgid "Un-named Color Scheme"
msgstr "Izenik gabeko kolore-eskema"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:468
msgid "Accessible Color Scheme"
msgstr "Erabilgarritasunezko kolore-eskema"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:533
msgid "Open Link"
msgstr "Ireki esteka"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:534
msgid "Copy Link Address"
msgstr "Kopiatu estekaren helbidea"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:538
#, fuzzy
msgid "Send Email To…"
msgstr "Bidali posta elektronikoa honi..."

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:539
msgid "Copy Email Address"
msgstr "Kopiatu helbide elektronikoa"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:39
msgid "Match case"
msgstr "Bereizi maiuskula/minuskulak"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:45
msgid "Regular expression"
msgstr "Adierazpen erregularra"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:49
msgid "Higlight all matches"
msgstr "Nabarmendu bat etortzeak"

#: ../src/plugin/qmltermwidget/lib/Vt102Emulation.cpp:982
msgid ""
"No keyboard translator available.  The information needed to convert key "
"presses into characters to send to the terminal is missing."
msgstr ""
"Ez dago teklatuaren itzultzailerik erabilgarri. Teklen sakatzeak terminalera "
"bidaliko diren karaktereetan bihurtzeko behar den informazioa falta da."

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:406
msgid "Color Scheme Error"
msgstr "Kolore-eskemaren errorea"

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:407
#, qt-format
msgid "Cannot load color scheme: %1"
msgstr "Ezin izan da kolore-eskema kargatu: %1"

#~ msgid "Color Scheme"
#~ msgstr "Kolore-eskema"

#~ msgid "FNS"
#~ msgstr "FNS"

#~ msgid "SCR"
#~ msgstr "PANT"

#~ msgid "CMD"
#~ msgstr "AGT"

#~ msgid "ALT"
#~ msgstr "ALT"

#~ msgid "SHIFT"
#~ msgstr "SHIFT"

#~ msgid "ESC"
#~ msgstr "ESC"

#~ msgid "PG_UP"
#~ msgstr "ORR_GO"

#~ msgid "PG_DN"
#~ msgstr "ORR_BE"

#~ msgid "DEL"
#~ msgstr "EZAB"

#~ msgid "HOME"
#~ msgstr "ETXEA"

#~ msgid "END"
#~ msgstr "AMAIERA"

#~ msgid "TAB"
#~ msgstr "TAB"

#~ msgid "ENTER"
#~ msgstr "ENTER"

#~ msgid "Login"
#~ msgstr "Saioa hasi"

#~ msgid "Require login"
#~ msgstr "Saioa hastea beharrezkoa"

#~ msgid "Enable or disable login at start"
#~ msgstr "Gaitu edo ezgaitu saio hasiera hastapenean"

#~ msgid "Ok"
#~ msgstr "Ados"

#~ msgid "Settings"
#~ msgstr "Ezarpenak"

#~ msgid "Required"
#~ msgstr "Beharrezkoa"

#~ msgid "Not required"
#~ msgstr "Ez da beharrezkoa"

#~ msgid "Show Keyboard Bar"
#~ msgstr "Erakutsi teklatu-barra"

#~ msgid "Enter password:"
#~ msgstr "Sartu pasahitza:"

#~ msgid "password"
#~ msgstr "pasahitza"
